# Devanagari Transliterators

Convert among common Devanagari transliteration systems. The following codecs are currently implemented.

| System         | Code     | Decoder  | Encoder  |
|:---------------|:--------:|:--------:|:--------:|
| Devanagari     | `dev`    | &#10003; | &#10003; |
| Hardvard Kyoto | `hk`     | &#10003; | &#10003; |
| IAST           | `iast`   | &#10003; | &#10003; |
| ITRANS         | `itrans` | &#10003; | &#10003; |
| SLP1           | `slp`    | &#10003; | &#10003; |
| (La)TeX        | `tex`    | &#10007; | &#10003; |
| Velthuis       | `vel`    | &#10003; | &#10003; |
| WX             | `wx`     | &#10003; | &#10003; |

## Installation

The best method to install is via `pip`.

```sh
pip3 install devtrans
```

Clone the [GitLab](https://gitlab.com/gaudha/devanagari-transliterators) repository to install from the source.

## Usage

### As a script

```sh
transliterate -d dev -e iast -i input_file -o outputfile
```
The options
- `-d` Decoder system code (defaults to`dev`)
- `-e` Encoder system code (defaults to `iast`)
- `-i` Input file (defaults to `STDIN`)
- `-o` Output file (defaults to `STDOUT`)

See help for more details
```sh
transliterate --help
```

### As a module

Import the module.

```python
import devtrans
```

Call any codec function with appropriate input text
```python
target = devtrans.wx2iast('xevanAgarI')
```

Devanagari encoders can have an optional boolean argument. If `True`, which is default, the encoder transliterates digits to Devanagari digits. If `False`, it leaves digits as it is.
```python
target = devtrans.wx2dev('BagavaxgIwA 18.78', False)
```

## Disclaimer

It has primarily been developed for personal use as a tea-time activity. Some of my colleagues and I are using it since 2015 for general texts from classical literature. I have been tweaking and making modifications whenever we found an issue. However, the transliterators are not tested exhaustively or thoroughly with various kinds of texts.

Majority of the transliteration schemes are case-sensitive. Currently, encoder/decoder for case-insensitve schemes works with lowercase characers only.

Many schemes use punctuation characters to represent letters which are represented using alphabets in another scheme. This makes punctuated text to be poorly transliterated. No character-escape mechanism is used.

## Contribute

Find the repo on [GitLab](https://gitlab.com/gaudha/devanagari-transliterators). If found bugs, please fix and push them, raise an [issue](https://gitlab.com/gaudha/devanagari-transliterators/issues) or mail [me](mailto:zombiechum@gmail.com).
