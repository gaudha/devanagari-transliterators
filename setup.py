#!/usr/bin/env python3

import setuptools

with open("README.md") as ifile:
    desc = ifile.read()

setuptools.setup(
    name='devtrans',
    version='3.0',
    scripts=['transliterate'],
    py_modules=['devtrans'],
    author='Sanal Vikram',
    author_email='zombiechum@gmail.com',
    license='MIT',
    description='Convert among Devanagari transliteration systems',
    long_description=desc,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/gaudha/devanagari-transliterators',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ]
)
